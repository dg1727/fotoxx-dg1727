.TH FOTOXX 1 2017-01-01 "Linux" "Fotoxx man page"

.SH NAME
 Fotoxx - photo/image editor and collection manager

.SH SYNOPSIS
 \fBFotoxx\fR [ \fBoptions\fR ] [ \fIfile\fR | \fIdirectory\fR ]

.SH DESCRIPTION
 Organize and administer a collection of images, edit images, 
 search images, and perform utility functions.
 Fotoxx is a GTK application which operates in its own window. 

.SH OVERVIEW

Image edit functions include:
 - View and edit most image and RAW file types
 - Adjust brightness/color/contrast using movable curves
 - Edit the brightness distribution directly for best balance
 - Trim (crop), Resize, Flip, Rotate (any angle)
 - Sharpen, Blur, Reduce noise, Remove red-eyes
 - Tone Mapping (enhance local contrast and faint details)
 - Auto adjust white balance, Match colors to a standard
 - Remove a color caste that may vary across the image
 - Fix vignetting and other brightness uniformity problems
 - Warp (stretch/distort) an image by dragging the mouse
 - Record edits for one image, apply to any number of images
 - Edit selected objects or areas separately within an image
 - Copy and paste selected objects or areas across images
 - Art effects (drawing, painting, cartoon, 3D relief, others)
 - Edit and apply custom convolution kernels
 - Pixel edit with variable brush transparency and blending
 - Paint retouch functions locally and gradually with the mouse
 - Smart erase: remove ground litter, power lines, etc.
 - Erase dust: remove dust spots on images from scanned slides
 - Flatten photo of a curved book page, stretch compressed text
 - Write text on an image (font/color/transparency/shadow/angle)
 - Montage: Arrange images and formatted text in a layout
 - Panorama, HDR and Stack composites (hand-held camera OK)
 - Combine multiple photos with varying focus depths (HDF)
 - Use Gimp, ImageMagick, etc. as plug-in functions

Utility functions include:
 - Context aware help available via the F1 key
 - Thumbnail browser and navigator, click image to view/edit
 - Custom graphic menu with user-selected content and layout
 - Assign custom keyboard shortcuts to menu functions
 - Bookmark image locations, jump to bookmarked location
 - Edit any image metadata (EXIF/IPTC tags, dates, comments...)
 - Convert image color profile (e.g. sRGB <-> Adobe RGB)
 - Search and report any metadata (thumbnails with text)
 - Use geotags from camera GPS, add/edit geotags manually
 - Search images by time, location, tags ... any metadata
 - World map at any scale, click on a marker for image gallery
 - Batch convert format, resize, move, export, burn DVD/BlueRay
 - Batch rename images, insert dates and sequence numbers
 - Batch convert RAW files to tiff-8/16, png-8/16, jpeg
 - Print image at any size for any paper format
 - Calibrate printer for more accurate printed colors
 - Create albums (views), select and arrange images
 - Slide-show with animated transitions and image zoom-in
 - Monitor color and gamma test patterns

.SH OPTIONS
Command line options
 \fB-v\fR                 print version and exit
 \fB-lang\fR \fIcode\fR   specify language (de, fr ...)
 \fB-prev\fR              open last file from the previous session
 \fB-recent\fR            show a gallery of most recent files 
 \fIfile\fR               initial image file to view or edit
 \fIdirectory\fR          initial directory for image gallery

.SH SEE ALSO
 The online user manual is available using the Help menu.
 This manual explains Fotoxx operation in great detail.
 The home page for Fotoxx is at http://kornelix.net

.SH AUTHORS
 Written by Mike Cornelison <kornelix@posteo.de>
 http://kornelix.net

