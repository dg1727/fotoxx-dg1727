# fotoxx makefile

FOTOXX = fotoxx-17.01.2.cc

# defaults for parameters that may be pre-defined
CXXFLAGS += -Wall -ggdb -fstack-protector-all
PREFIX ?= /usr
CPPFLAGS ?= -O2

# target install directories
BINDIR = $(PREFIX)/bin
SHAREDIR = $(PREFIX)/share/fotoxx
DATADIR = $(SHAREDIR)/data
ICONDIR = $(SHAREDIR)/icons
LOCALESDIR = $(SHAREDIR)/locales
DOCDIR = $(PREFIX)/share/doc/fotoxx
MANDIR = $(PREFIX)/share/man/man1
APPDATADIR = $(PREFIX)/share/appdata
MENUFILE = $(PREFIX)/share/applications/fotoxx.desktop

CFLAGS = $(CXXFLAGS) $(CPPFLAGS) -c                \
   `pkg-config --cflags gtk+-3.0`                  \
   -I/usr/include/clutter-1.0/                     \
   -I/usr/include/cogl/                            \
   -I/usr/include/json-glib-1.0/                   \
   -I/usr/include/clutter-gtk-1.0/                 \
   -I/usr/include/libchamplain-gtk-0.12/           \
   -I/usr/include/libchamplain-0.12/

LIBS = `pkg-config --libs gtk+-3.0` -lrt -lpthread -ltiff -lpng -lraw -llcms2    \
         -lclutter-1.0 -lclutter-gtk-1.0 -lchamplain-0.12 -lchamplain-gtk-0.12

ALLFILES = fotoxx.o f.widgets.o f.image.o f.file.o f.gallery.o f.gmenu.o         \
           f.area.o f.meta.o f.edit.o f.repair.o f.warp.o f.effects.o            \
           f.combine.o f.mashup.o f.tools.o f.process.o zfuncs.o

fotoxx: $(ALLFILES)
	$(CXX) $(LDFLAGS) -o fotoxx $(ALLFILES) $(LIBS) \

fotoxx.o: $(FOTOXX) fotoxx.h
	$(CXX) -o fotoxx.o $(FOTOXX) $(CFLAGS) \

f.widgets.o: f.widgets.cc  fotoxx.h
	$(CXX) f.widgets.cc $(CFLAGS) \

f.image.o: f.image.cc  fotoxx.h
	$(CXX) f.image.cc $(CFLAGS) \

f.file.o: f.file.cc  fotoxx.h
	$(CXX) f.file.cc $(CFLAGS) \

f.gallery.o: f.gallery.cc  fotoxx.h
	$(CXX) f.gallery.cc $(CFLAGS) \

f.gmenu.o: f.gmenu.cc  fotoxx.h
	$(CXX) f.gmenu.cc $(CFLAGS) \

f.area.o: f.area.cc  fotoxx.h
	$(CXX) f.area.cc $(CFLAGS) \

f.meta.o: f.meta.cc  fotoxx.h
	$(CXX) f.meta.cc $(CFLAGS) \

f.edit.o: f.edit.cc  fotoxx.h
	$(CXX) f.edit.cc $(CFLAGS) \

f.repair.o: f.repair.cc  fotoxx.h
	$(CXX) f.repair.cc $(CFLAGS) \

f.warp.o: f.warp.cc  fotoxx.h
	$(CXX) f.warp.cc $(CFLAGS) \

f.effects.o: f.effects.cc  fotoxx.h
	$(CXX) f.effects.cc $(CFLAGS) \

f.combine.o: f.combine.cc  fotoxx.h
	$(CXX) f.combine.cc $(CFLAGS) \

f.mashup.o: f.mashup.cc  fotoxx.h
	$(CXX) f.mashup.cc $(CFLAGS) \

f.tools.o: f.tools.cc  fotoxx.h
	$(CXX) f.tools.cc $(CFLAGS) \

f.process.o: f.process.cc  fotoxx.h
	$(CXX) f.process.cc $(CFLAGS) \

zfuncs.o: zfuncs.cc  zfuncs.h
	$(CXX) zfuncs.cc $(CFLAGS) -D DOCDIR=\"$(DOCDIR)\" \

install: fotoxx uninstall
	mkdir -p  $(DESTDIR)$(BINDIR)
	mkdir -p  $(DESTDIR)$(DATADIR)
	mkdir -p  $(DESTDIR)$(ICONDIR)
	mkdir -p  $(DESTDIR)$(LOCALESDIR)
	mkdir -p  $(DESTDIR)$(DOCDIR)
	mkdir -p  $(DESTDIR)$(MANDIR)
	mkdir -p  $(DESTDIR)$(PREFIX)/share/applications
	mkdir -p  $(DESTDIR)$(APPDATADIR)
	cp -f  fotoxx $(DESTDIR)$(BINDIR)
	cp -f -R  data/* $(DESTDIR)$(DATADIR)
	cp -f -R  icons/* $(DESTDIR)$(ICONDIR)
	cp -f -R  locales/* $(DESTDIR)$(LOCALESDIR)
	cp -f -R  doc/* $(DESTDIR)$(DOCDIR)
	gzip -f -9 $(DESTDIR)$(DOCDIR)/changelog
	cp -f -R  appdata/* $(DESTDIR)$(APPDATADIR)
	# man page
	cp -f doc/fotoxx.man fotoxx.1
	gzip -f -9 fotoxx.1
	cp fotoxx.1.gz $(DESTDIR)$(MANDIR)
	rm -f fotoxx.1.gz
	# menu (desktop) file
	cp -f desktop $(DESTDIR)$(MENUFILE)

uninstall:
	rm -f  $(DESTDIR)$(BINDIR)/fotoxx
	rm -f -R  $(DESTDIR)$(SHAREDIR)
	rm -f -R  $(DESTDIR)$(DOCDIR)
	rm -f  $(DESTDIR)$(MANDIR)/fotoxx.1.gz
	rm -f  $(DESTDIR)$(MENUFILE)

clean:
	rm -f  fotoxx
	rm -f  *.o


