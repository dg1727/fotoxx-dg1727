/********************************************************************************

   Fotoxx      edit photos and manage collections  

   Copyright 2007-2017 Michael Cornelison
   Source URL: http://kornelix.net
   Contact: kornelix@posteo.de

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program. If not, see http://www.gnu.org/licenses/.

*********************************************************************************

   Fotoxx window and menu build functions

   build_widgets           build widgets and menus for F/G/W/M view modes
   m_viewmode              set current F/G/W/M view mode
   popup_menufunc          image/thumb right-click menu func
   image_Rclick_popup      popup menu for image right-click
   gallery_Lclick_func     thumbnail left-click function
   gallery_Rclick_popup    popup menu for thumbnail right-click
   m_favorites             function to generate favorites menu
   favorites_callback      response function for clicked menu

*********************************************************************************/

#define EX extern                                                                //  enable extern declarations
#include "fotoxx.h"                                                              //  (variables in fotoxx.h are defined)

/********************************************************************************/

GtkWidget   *mFile, *mTools, *mProc, *mGmenu, *mMeta, *mArea;
GtkWidget   *mEdit, *mRep, *mWarp, *mEff, *mComb, *mHelp;

GtkWidget   *popmenu_main, *popmenu_thumb, *popmenu_album, *popmenu_raw;
int         NFmenu, NGmenu, NWmenu, NMmenu;


//  initialize widgets and menus for F/G/W/M view modes
//  called from main() before gtk_main() loop is entered

void build_widgets()
{
   Mwin = gtk_window_new(GTK_WINDOW_TOPLEVEL);                                   //  create main window
   gtk_window_set_title(MWIN,Frelease);
   MWvbox = gtk_box_new(VERTICAL,0);                                             //  top container
   gtk_container_add(GTK_CONTAINER(Mwin),MWvbox);
   gtk_widget_show_all(Mwin);

   G_SIGNAL(Mwin,"delete_event",delete_event,0);                                 //  connect signals to main window
   G_SIGNAL(Mwin,"destroy",destroy_event,0);
   G_SIGNAL(Mwin,"window-state-event",state_event,0);
   G_SIGNAL(Mwin,"key-press-event",KBpress,0);                                   //  connect KB events to main window
   G_SIGNAL(Mwin,"key-release-event",KBrelease,0);                               //  16.11

   //  get hardware and set default font for screen widgets
   
   get_hardware_info(Mwin);
   zsetfont(dialog_font);

   //  F view widgets - image file

   Fhbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Fhbox,1,1,0);
   Fmenu = gtk_box_new(VERTICAL,0);                                              //  left vbox for vert. menu
   gtk_box_pack_start(GTK_BOX(Fhbox),Fmenu,0,0,0);
   Fvbox = gtk_box_new(VERTICAL,0);                                              //  right vbox for image
   gtk_box_pack_start(GTK_BOX(Fhbox),Fvbox,1,1,0);
   Fpanel = gtk_box_new(HORIZONTAL,0);                                           //  panel over image
   gtk_box_pack_start(GTK_BOX(Fvbox),Fpanel,0,0,0);
   gtk_widget_set_size_request(Fpanel,0,20);
   Fpanlab = gtk_label_new("panel");
   gtk_box_pack_start(GTK_BOX(Fpanel),Fpanlab,0,0,0);
   Fpanelshow = 1;                                                               //  panel normally shows               16.02
   Fdrawin = gtk_drawing_area_new();                                             //  image drawing area
   gtk_box_pack_start(GTK_BOX(Fvbox),Fdrawin,1,1,0);
   gtk_widget_hide(Fhbox);

   gtk_widget_add_events(Fdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to image window
   gtk_widget_add_events(Fdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Fdrawin,GDK_BUTTON_MOTION_MASK);
   gtk_widget_add_events(Fdrawin,GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(Fdrawin,GDK_SCROLL_MASK);
   G_SIGNAL(Fdrawin,"button-press-event",mouse_event,0);                         //  connect signals
   G_SIGNAL(Fdrawin,"button-release-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"motion-notify-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"scroll-event",mouse_event,0);
   G_SIGNAL(Fdrawin,"draw",Fpaint,0);
   drag_drop_dest(Fdrawin,drop_event);                                           //  accept drag-drop file              16.11

   //  G view widgets - thumbnail gallery

   Ghbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Ghbox,1,1,0);
   Gmenu = gtk_box_new(VERTICAL,0);                                              //  left vbox for vert. menu
   gtk_box_pack_start(GTK_BOX(Ghbox),Gmenu,0,0,0);
   Gvbox = gtk_box_new(VERTICAL,0);                                              //  right vbox for gallery
   gtk_box_pack_start(GTK_BOX(Ghbox),Gvbox,1,1,0);
   Gpanel = gtk_box_new(HORIZONTAL,0);                                           //  top panel for [TOP] and navi buttons
   gtk_box_pack_start(GTK_BOX(Gvbox),Gpanel,0,0,2);
   Galbum = gtk_button_new_with_label(ZTX("Album"));                             //  [Album] button in panel
   gtk_box_pack_start(GTK_BOX(Gpanel),Galbum,0,0,3);
   Gtop = gtk_button_new_with_label(ZTX("TOP"));                                 //  [TOP] button in panel
   gtk_box_pack_start(GTK_BOX(Gpanel),Gtop,0,0,3);
   Gsep = gtk_label_new(0);
   gtk_label_set_markup(GTK_LABEL(Gsep),"<span font=\"sans bold 12\" >@</span>");
   gtk_box_pack_start(GTK_BOX(Gpanel),Gsep,0,0,10);

   Gsep = gtk_separator_new(HORIZONTAL);                                         //  separator line
   gtk_box_pack_start(GTK_BOX(Gvbox),Gsep,0,0,3);
   Gscroll = gtk_scrolled_window_new(0,0);                                       //  scrolled window for gallery
   gtk_scrolled_window_set_policy(SCROLLWIN(Gscroll),NEVER,ALWAYS);
   Gadjust = gtk_scrolled_window_get_vadjustment(SCROLLWIN(Gscroll));
   gtk_box_pack_start(GTK_BOX(Gvbox),Gscroll,1,1,0);
   Gdrawin = gtk_drawing_area_new();                                             //  gallery drawing area
   gtk_container_add(GTK_CONTAINER(Gscroll),Gdrawin);
   gtk_widget_hide(Ghbox);

   gtk_widget_add_events(Gdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to gallery window
   gtk_widget_add_events(Gdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Gdrawin,GDK_POINTER_MOTION_MASK);
   G_SIGNAL(Gtop,"clicked",navi::newtop,0);
   G_SIGNAL(Galbum,"clicked",navi::newalbum,0);
   G_SIGNAL(Gdrawin,"button-press-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"button-release-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"motion-notify-event",navi::mouse_event,0);
   G_SIGNAL(Gdrawin,"draw",navi::gallery_paint,null);
   drag_drop_source(Gdrawin,navi::gallery_dragfile);                             //  start file drag-drop               16.08
   drag_drop_dest(Gdrawin,navi::gallery_dropfile);                               //  accept drag-drop file              16.08

   //  W view widgets - local map files

   Whbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Whbox,1,1,0);
   Wmenu = gtk_box_new(VERTICAL,0);                                              //  left vbox for vert. menu
   gtk_box_pack_start(GTK_BOX(Whbox),Wmenu,0,0,0);
   Wvbox = gtk_box_new(VERTICAL,0);                                              //  right vbox for filemap
   gtk_box_pack_start(GTK_BOX(Whbox),Wvbox,1,1,0);
   Wdrawin = gtk_drawing_area_new();                                             //  filemap drawing area
   gtk_box_pack_start(GTK_BOX(Wvbox),Wdrawin,1,1,0);
   gtk_widget_hide(Whbox);

   gtk_widget_add_events(Wdrawin,GDK_BUTTON_PRESS_MASK);                         //  connect mouse events to filemap window
   gtk_widget_add_events(Wdrawin,GDK_BUTTON_RELEASE_MASK);
   gtk_widget_add_events(Wdrawin,GDK_BUTTON_MOTION_MASK);
   gtk_widget_add_events(Wdrawin,GDK_POINTER_MOTION_MASK);
   gtk_widget_add_events(Wdrawin,GDK_SCROLL_MASK);
   G_SIGNAL(Wdrawin,"button-press-event",mouse_event,0);                         //  connect signals
   G_SIGNAL(Wdrawin,"button-release-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"motion-notify-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"scroll-event",mouse_event,0);
   G_SIGNAL(Wdrawin,"draw",Fpaint,0);

   //  M view widgets - internet maps                                            //  16.05

   Mhbox = gtk_box_new(HORIZONTAL,0);                                            //  top container
   gtk_box_pack_start(GTK_BOX(MWvbox),Mhbox,1,1,0);
   Mmenu = gtk_box_new(VERTICAL,0);                                              //  left vbox for vert. menu
   gtk_box_pack_start(GTK_BOX(Mhbox),Mmenu,0,0,0);
   Mvbox = gtk_box_new(VERTICAL,0);                                              //  right vbox for net map
   gtk_box_pack_start(GTK_BOX(Mhbox),Mvbox,1,1,0);
   gtk_widget_hide(Mhbox);


   //  menu popup text (tool tips) ----------------------------------------
   
   //  F/G/W/M view menu buttons

   cchar * imagefile_tip = ZTX("Current Image File (key F)");
   cchar * gallery_tip = ZTX("Thumbnail Gallery (key G)");
   cchar * worldmaps_tip = ZTX("World Maps (key W)");
   cchar * netmaps_tip = ZTX("Net Maps (key M)");                                //  16.05

   //  F view menu buttons
   cchar * favorites_tip = ZTX("Favorite Functions");
   cchar * file_tip = ZTX("File: Open, RAW, Rename, Delete, Print");
   cchar * save_tip = ZTX("Save modified image file to disk");
   cchar * prev_next_tip = ZTX("Open previous or next file (left/right mouse click)");
   cchar * metadata_tip = ZTX("Metadata: Captions, Tags, Ratings, Geotags, Search ... ");
   cchar * areas_tip = ZTX("Areas: Select areas to edit, copy and paste");
   cchar * edit_tip = ZTX("Edit: Trim, Rotate, Resize, Brightness, Contrast, Text ...");
   cchar * repair_tip = ZTX("Repair: Sharpen, Noise, Red-eyes, Color, Paint, Clone ...");
   cchar * warp_tip = ZTX("Warp: Fix Perspective, Warp or unwarp image ...");
   cchar * effects_tip = ZTX("Effects: Special Effects, Arty Transforms");
   cchar * combine_tip = ZTX("Combine: HDR, HDF, Panorama, Stack, Mashup");
   cchar * undo_redo_tip = ZTX("left/right click: undo/redo 1 edit \n"
                               " with key A: undo/redo all edits \n"
                               " middle click: go to any prior edit");
   cchar * process_tip = ZTX("Process: batch convert, batch metadata, image search ...");
   cchar * tools_tip = ZTX("Tools: Index, Settings, Shortcuts, Magnify ...");
   cchar * help_tip = ZTX("Help: Quick Start, User Guide, Recent Changes ...");

   //  G view menu buttons
   cchar * gmenu_tip = ZTX("Sync Gallery, Export, Flickr, Albums, Slide Show");
   cchar * bookmarks_tip = ZTX("go to bookmarked image");
   cchar * thumb_increase_tip = ZTX("increase thumbnail size");
   cchar * thumb_reduce_tip = ZTX("reduce thumbnail size");
   cchar * sort_order_tip = ZTX("change sort order");
   cchar * jump_begin_tip = ZTX("jump to beginning (top)");
   cchar * jump_end_tip = ZTX("jump to end (bottom)");
   cchar * previous_page_tip = ZTX("previous page");
   cchar * next_page_tip = ZTX("next page");
   cchar * slow_scroll_tip = ZTX("slow scroll");

   //  W view menu buttons
   cchar * load_filemap_tip = ZTX("Choose a map file");
   cchar * mark_filemap_tip = ZTX("Set all images or current gallery");

   //  M view menu buttons                                                       //  16.05
   cchar * netmap_source_tip = ZTX("Choose Internet map source");
   cchar * mark_netmap_tip = ZTX("Set map markers from current gallery");

   //  file popup menu
   cchar * clone_tip = ZTX("Open another window");
   cchar * open_tip = ZTX("Open a new image file");
   cchar * prevfiles_tip = ZTX("Open the previously seen file");
   cchar * recentfiles_tip = ZTX("Open a recently seen file");
   cchar * newfiles_tip = ZTX("Open a newly added file");
   cchar * rawtherapee_tip = ZTX("Open and edit a camera RAW file");
   cchar * rename_tip = ZTX("Change the image file name");
   cchar * copy_move_tip = ZTX("Copy or Move an image file to a new location");
   cchar * copyto_desktop_tip = ZTX("Copy an image file to the desktop");
   cchar * create_tip = ZTX("Create a blank image");
   cchar * deltrash_tip = ZTX("Delete or trash image file");
   cchar * print_tip = ZTX("Print the current image");
   cchar * print_calibrated_tip = ZTX("Print current image with adjusted colors");
   cchar * quit_tip = ZTX("Quit Fotoxx");

   //  metadata popup menu
   cchar * meta_view_short_tip = ZTX("List a few key metadata items");
   cchar * meta_view_long_tip = ZTX("List all metadata items");
   cchar * captions_tip = ZTX("(Toggle) show captions and comments");
   cchar * edit_metadata_tip = ZTX("Edit image tags/geotags/caption/rating ...");
   cchar * meta_edit_any_tip = ZTX("Edit any image metadata");
   cchar * meta_delete_tip = ZTX("Remove selected image metadata");
   cchar * batch_tags_tip = ZTX("Add/remove tags for multiple images");
   cchar * batch_rename_tags_tip = ZTX("Convert tag names for all images");
   cchar * batch_change_metadata_tip = ZTX("Add/change/delete metadata for multiple images");
   cchar * batch_report_metadata_tip = ZTX("Report metadata for multiple images");
   cchar * batch_geotags_tip = ZTX("Add/revise geotags for multiple images");
   cchar * locations_tip = ZTX("Find all images for a location [date]");
   cchar * timeline_tip = ZTX("Show image counts by month, select and report");
   cchar * search_images_tip = ZTX("Find images meeting select criteria");

   //  select area popup menu
   cchar * select_tip = ZTX("Select object or area for editing");
   cchar * select_find_gap_tip = ZTX("Find a gap in an area outline");
   cchar * select_hairy_tip = ZTX("Select hairy or irregular edge");
   cchar * select_show_tip = ZTX("Show (outline) existing area");
   cchar * select_hide_tip = ZTX("Hide existing area");
   cchar * select_enable_tip = ZTX("Enable area for editing");
   cchar * select_disable_tip = ZTX("Disable area for editing");
   cchar * select_invert_tip = ZTX("Reverse existing area");
   cchar * select_unselect_tip = ZTX("Erase existing area");
   cchar * select_copy_tip = ZTX("Copy area for later pasting into image");
   cchar * select_save_tip = ZTX("Save area to a file with transparency");
   cchar * select_open_tip = ZTX("Open a file and paste as area into image");
   cchar * select_paste_tip = ZTX("Paste previously copied area into image");

   //  edit popup menu
   cchar * trimrotate_tip = ZTX("Trim/Crop margins and/or Rotate");
   cchar * upright_tip = ZTX("Upright a rotated image");
   cchar * voodoo1_tip = ZTX("Fast auto enhance that may work OK");
   cchar * voodoo2_tip = ZTX("Fast auto enhance that may work OK");
   cchar * combo_tip = ZTX("Adjust brightness, contrast, color");
   cchar * tonemap_tip = ZTX("Add local contrast, enhance details");
   cchar * britedist_tip = ZTX("Edit brightness distribution");
   cchar * zonal_flatten_tip = ZTX("Flatten zonal brightness distribution");
   cchar * resize_tip = ZTX("Change pixel dimensions");
   cchar * flip_tip = ZTX("Mirror image horizontally or vertically");
   cchar * write_text_tip = ZTX("Write text on image");
   cchar * write_line_tip = ZTX("Write lines or arrows on image");
   cchar * paint_edits_tip = ZTX("Paint edit function gradually with mouse");
   cchar * lever_edits_tip = ZTX("Leverage edits by brightness or color");
   cchar * plugins_tip = ZTX("Edit plugins menu or run a plugin function");

   //  repair popup menu
   cchar * sharpen_tip = ZTX("Make the image look sharper");
   cchar * blur_tip = ZTX("Make the image look fuzzy");
   cchar * denoise_tip = ZTX("Filter noise from low-light photos");
   cchar * smart_erase_tip = ZTX("Remove unwanted objects");
   cchar * redeye_tip = ZTX("Fix red-eyes from electronic flash");
   cchar * paint_clone_tip = ZTX("Paint or clone image pixels using the mouse");
   cchar * blend_image_tip = ZTX("Blend image pixels using the mouse");
   cchar * paint_transp_tip = ZTX("Paint image transparency using the mouse");
   cchar * add_transp_tip = ZTX("Add image transparency based on image attributes");
   cchar * color_mode_tip = ZTX("Make BW/color, negative/positive, sepia");
   cchar * shift_colors_tip = ZTX("Shift/convert colors into other colors");
   cchar * color_sat_tip = ZTX("Adjust color intensity (saturation)");
   cchar * adjust_RGB_tip = ZTX("Adjust color using RGB or CMY colors");
   cchar * adjust_HSL_tip = ZTX("Adjust color using HSL colors");
   cchar * bright_grad_tip = ZTX("Add a brightness/color gradient across the image");
   cchar * local_color_tip = ZTX("Adjust color in selected image areas");
   cchar * match_color_tip = ZTX("Match colors on one image with another");
   cchar * color_profile_tip = ZTX("Convert to another color profile");
   cchar * remove_dust_tip = ZTX("Remove dust spots from scanned slides");
   cchar * anti_alias_tip = ZTX("Smoothen edges with jaggies");
   cchar * color_fringes_tip = ZTX("Reduce Chromatic Abberation");
   cchar * stuck_pixels_tip = ZTX("Erase known hot and dark pixels");

   //  warp popup menu
   cchar * unbend_tip = ZTX("Remove curvature, esp. panoramas");
   cchar * perspective_tip = ZTX("Straighten objects seen from an angle");
   cchar * warp_area_tip = ZTX("Distort image areas using the mouse");
   cchar * unwarp_closeup_tip = ZTX("Unwarp closeup face photo to remove distortion");
   cchar * warp_curved_tip = ZTX("Distort the whole image using the mouse");
   cchar * warp_linear_tip = ZTX("Distort the whole image using the mouse");
   cchar * warp_affine_tip = ZTX("Distort the whole image using the mouse");
   cchar * flatbook_tip = ZTX("Flatten a photographed book page");
   cchar * sphere_tip = ZTX("Make a spherical projection of an image");
   cchar * selective_rescale_tip = ZTX("Rescale image outside selected areas");
   cchar * waves_tip = ZTX("Warp an image with a wave pattern");

   //  effects popup menu
   cchar * colordep_tip = ZTX("Reduce color depth (posterize)");
   cchar * sketch_tip = ZTX("Convert to pencil sketch");
   cchar * cartoon_tip = ZTX("Convert image into a cartoon drawing");
   cchar * linedraw_tip = ZTX("Convert to line drawing (edge detection)");
   cchar * colordraw_tip = ZTX("Convert to solid color drawing");
   cchar * gradblur_tip = ZTX("Graduated Blur depending on contrast");
   cchar * emboss_tip = ZTX("Create an embossed or 3D appearance");
   cchar * tiles_tip = ZTX("Convert to square tiles");
   cchar * dots_tip = ZTX("Convert to dots (Roy Lichtenstein effect)");
   cchar * painting_tip = ZTX("Convert into a simulated painting");
   cchar * vignette_tip = ZTX("Change brightness or color radially");
   cchar * texture_tip = ZTX("Add texture to an image");
   cchar * pattern_tip = ZTX("Tile image with a repeating pattern");
   cchar * mosaic_tip = ZTX("Create a mosaic with tiles made from all images");
   cchar * anykernel_tip = ZTX("Process an image using a custom kernel");
   cchar * dirblur_tip = ZTX("Blur an image in the direction of mouse drags");
   cchar * blur_BG_tip = ZTX("Increasing blur with distance from selected areas");

   //  combine popup menu
   cchar * HDR_tip = ZTX("Combine bright/dark images for better detail");
   cchar * HDF_tip = ZTX("Combine near/far focus images for deeper focus");
   cchar * STP_tip = ZTX("Combine images to erase passing people, etc.");
   cchar * STN_tip = ZTX("Combine noisy images into a low-noise image");
   cchar * pano_tip = ZTX("Combine images into a panorama");
   cchar * vpano_tip = ZTX("Combine images into a vertical panorama");
   cchar * pano_PT_tip = ZTX("Combine images into a panorama (panorama tools)");
   cchar * mashup_tip = ZTX("Arrange images and text in a layout (montage)");

   //  tools popup menu
   cchar * index_tip = ZTX("Index new files and make thumbnails");
   cchar * settings_tip = ZTX("Change user preferences");
   cchar * KBshortcuts_tip = ZTX("Change Keyboard Shortcut Keys");
   cchar * show_brdist_tip = ZTX("Show a brightness distribution graph");
   cchar * gridlines_tip = ZTX("Show or revise grid lines");
   cchar * line_color_tip = ZTX("Change color of foreground lines");
   cchar * show_RGB_tip = ZTX("Show RGB colors at mouse click");
   cchar * magnify_tip = ZTX("Magnify image around the mouse position");
   cchar * darkbrite_tip = ZTX("Highlight darkest and brightest pixels");
   cchar * moncolor_tip = ZTX("Chart to adjust monitor color");
   cchar * mongamma_tip = ZTX("Chart to adjust monitor gamma");
   cchar * changelang_tip = ZTX("Change the GUI language");
   cchar * untranslated_tip = ZTX("Report missing translations");
   cchar * calibrate_printer_tip = ZTX("Calibrate printer colors");
   cchar * setdesktop_tip = ZTX("Set desktop wallpaper from current Fotoxx image");
   cchar * cycledesktop_tip = ZTX("Cycle desktop wallpaper from a Fotoxx album");
   cchar * resources_tip = ZTX("Memory and CPU (to terminal/logfile)");
   cchar * zappcrash_tip = "deliberate crash with traceback dump";

   //  process popup menu
   cchar * batch_convert_tip = ZTX("Rename/convert/resize/move multiple files");
   cchar * batch_upright_tip = ZTX("Upright multiple rotated image files");
   cchar * batch_deltrash_tip = ZTX("Delete or Trash multiple files");
   cchar * batch_raw_tip = ZTX("Convert camera RAW files using libraw");                                            //  16.07
   cchar * batch_rawtherapee_tip = ZTX("Convert camera RAW files using Raw Therapee");
   cchar * scriptfiles_tip = ZTX("Build and run edit script files");
   cchar * burn_tip = ZTX("Burn selected image files to DVD/BlueRay disc");
   cchar * duplicates_tip = ZTX("Search all image files and report duplicates");
   cchar * export_filelist_tip = ZTX("Create a file of selected image files");
   //  metadata batch functions are duplicated here

   //  help popup menu
   cchar * quick_start_tip = ZTX("Quick Start mini-guide");
   cchar * user_guide_tip = ZTX("Read the user guide");
   cchar * user_guide_changes_tip = ZTX("Recent user guide changes");
   cchar * edit_funcs_summary_tip = ZTX("Summary of image edit functions");
   cchar * readme_tip = ZTX("Technical installation notes");
   cchar * changelog_tip = ZTX("List updates by Fotoxx version");
   cchar * logfile_tip = ZTX("View the log file and error messages");
   cchar * translations_tip = ZTX("How to do Fotoxx translations");
   cchar * homepage_tip = ZTX("Show the Fotoxx web page");
   cchar * about_tip = ZTX("Version, license, contact, credits");

   //  gallery popup menu
   cchar * alldirs_tip = ZTX("list all directories, click any for gallery view");
   cchar * sync_gallery_tip = ZTX("Set gallery from current image file");
   cchar * export_images_tip = ZTX("Export selected image files to a directory");
   cchar * flickr_upload_tip = ZTX("Upload image files to Flickr web service");
   cchar * manage_albums_tip = ZTX("Organize images into albums");
   cchar * update_albums_tip = ZTX("Update album files to latest version");
   cchar * replace_album_file_tip = ZTX("Replace album file with another file");
   cchar * slideshow_tip = ZTX("Start a slide show");


   //  build menu entries -------------------------------------------------

   #define MENUENT(_topmenu, _text, _icon, _desc, _func, _arg)    \
   {  me = Nmenus++;                                              \
      if (me >= maxmenus) zappcrash("maxmenus exceeded");         \
      menutab[me].topmenu = _topmenu;                             \
      menutab[me].menu = _text;                                   \
      menutab[me].icon = _icon;                                   \
      menutab[me].desc = _desc;                                   \
      menutab[me].func = _func;                                   \
      if (_arg) menutab[me].arg = _arg;                           \
      else menutab[me].arg = _text;                               \
   }

   int      me;
   Nmenus = 0;
   
   //  build popup menus first

   mFile = create_popmenu();
   MENUENT(mFile,    ZTX("New Window"), 0,                  clone_tip,              m_clone, 0 );
   MENUENT(mFile,    ZTX("Sync Gallery"), 0,                sync_gallery_tip,       m_sync_gallery, 0 );
   MENUENT(mFile,    ZTX("Recently Seen Images"), 0,        recentfiles_tip,        m_recentfiles, 0 );
   MENUENT(mFile,    ZTX("Newest Images"), 0,               newfiles_tip,           m_newfiles, 0 );
   MENUENT(mFile,    ZTX("Open Image File"), 0,             open_tip,               m_open, 0 );
   MENUENT(mFile,    ZTX("Open Previous File"), 0,          prevfiles_tip,          m_previous, 0 );
   MENUENT(mFile,    ZTX("Open RAW (Raw Therapee)"), 0,     rawtherapee_tip,        m_rawtherapee, 0 );
   MENUENT(mFile,    ZTX("New Blank Image"), 0,             create_tip,             m_create, 0 );
   MENUENT(mFile,    ZTX("Rename Image File"), 0,           rename_tip,             m_rename, 0 );
   MENUENT(mFile,    ZTX("Copy/Move to Location"), 0,       copy_move_tip,          m_copy_move, 0 );
   MENUENT(mFile,    ZTX("Copy to Desktop"), 0,             copyto_desktop_tip,     m_copyto_desktop, 0 );
   MENUENT(mFile,    ZTX("Delete/Trash Image File"), 0,     deltrash_tip,           m_delete_trash, 0 );
   MENUENT(mFile,    ZTX("Print Image"), 0,                 print_tip,              m_print, 0 );
   MENUENT(mFile,    ZTX("Print Calibrated Image"), 0,      print_calibrated_tip,   m_print_calibrated, 0 );
   MENUENT(mFile,    ZTX("Quit Fotoxx"), 0,                 quit_tip,               m_quit, 0 );

   mMeta = create_popmenu();
   MENUENT(mMeta,    ZTX("View Metadata (short)"), 0,       meta_view_short_tip,          m_meta_view_short, 0 );
   MENUENT(mMeta,    ZTX("View Metadata (long)"), 0,        meta_view_long_tip,           m_meta_view_long, 0 );
   MENUENT(mMeta,    ZTX("Show Captions on Image"), 0,      captions_tip,                 m_captions, 0 );
   MENUENT(mMeta,    ZTX("Edit Metadata"), 0,               edit_metadata_tip,            m_edit_metadata, 0 );
   MENUENT(mMeta,    ZTX("Edit Any Metadata"), 0,           meta_edit_any_tip,            m_meta_edit_any, 0 );
   MENUENT(mMeta,    ZTX("Delete Metadata"), 0,             meta_delete_tip,              m_meta_delete, 0 );
   MENUENT(mMeta,    ZTX("Batch Add/Remove Tags"), 0,       batch_tags_tip,               m_batch_tags, 0 );
   MENUENT(mMeta,    ZTX("Batch Rename Tags"), 0,           batch_rename_tags_tip,        m_batch_rename_tags, 0 );
   MENUENT(mMeta,    ZTX("Batch Add/Change Metadata"), 0,   batch_change_metadata_tip,    m_batch_change_metadata, 0 );
   MENUENT(mMeta,    ZTX("Batch Report Metadata"), 0,       batch_report_metadata_tip,    m_batch_report_metadata, 0 );
   MENUENT(mMeta,    ZTX("Batch Geotags"), 0,               batch_geotags_tip,            m_batch_geotags, 0 );
   MENUENT(mMeta,    ZTX("Image Locations/Dates"), 0,       locations_tip,                m_locations, 0 );
   MENUENT(mMeta,    ZTX("Image Timeline"), 0,              timeline_tip,                 m_timeline, 0 );
   MENUENT(mMeta,    ZTX("Search Images"), 0,               search_images_tip,            m_search_images, 0 );

   mArea = create_popmenu();
   MENUENT(mArea,    ZTX("Select"), 0,             select_tip,                m_select, 0 );
   MENUENT(mArea,    ZTX("Find Gap"), 0,           select_find_gap_tip,       m_select_find_gap, 0 );
   MENUENT(mArea,    ZTX("Select Hairy"), 0,       select_hairy_tip,          m_select_hairy, 0);
   MENUENT(mArea,    ZTX("Show"), 0,               select_show_tip,           m_select_show, 0 );
   MENUENT(mArea,    ZTX("Hide"), 0,               select_hide_tip,           m_select_hide, 0 );
   MENUENT(mArea,    ZTX("Enable"), 0,             select_enable_tip,         m_select_enable, 0 );
   MENUENT(mArea,    ZTX("Disable"), 0,            select_disable_tip,        m_select_disable, 0 );
   MENUENT(mArea,    ZTX("Invert"), 0,             select_invert_tip,         m_select_invert, 0 );
   MENUENT(mArea,    ZTX("Unselect"), 0,           select_unselect_tip,       m_select_unselect, 0 );
   MENUENT(mArea,    ZTX("Copy Area"), 0,          select_copy_tip,           m_select_save, 0 );
   MENUENT(mArea,    ZTX("Paste Area"), 0,         select_paste_tip,          m_select_paste, 0 );
   MENUENT(mArea,    ZTX("Open Area File"), 0,     select_open_tip,           m_select_open, 0 );
   MENUENT(mArea,    ZTX("Save Area File"), 0,     select_save_tip,           m_select_save, 0 );

   mEdit = create_popmenu(); 
   MENUENT(mEdit,    ZTX("Trim/Rotate"), 0,              trimrotate_tip,            m_trimrotate, 0 );
   MENUENT(mEdit,    ZTX("Upright"), 0,                  upright_tip,               m_upright, 0 );
   MENUENT(mEdit,    ZTX("Voodoo 1"), 0,                 voodoo1_tip,               m_voodoo1, 0 );
   MENUENT(mEdit,    ZTX("Voodoo 2"), 0,                 voodoo2_tip,               m_voodoo2, 0);
   MENUENT(mEdit,    ZTX("Retouch Combo"), 0,            combo_tip,                 m_combo, 0 );
   MENUENT(mEdit,    ZTX("Edit Brightness"), 0,          britedist_tip,             m_britedist, 0 );
   MENUENT(mEdit,    ZTX("Zonal Flatten"), 0,            zonal_flatten_tip,         m_zonal_flatten, 0 );
   MENUENT(mEdit,    ZTX("Tone Mapping"), 0,             tonemap_tip,               m_tonemap, 0 );
   MENUENT(mEdit,    ZTX("Resize"), 0,                   resize_tip,                m_resize, 0 );
   MENUENT(mEdit,    ZTX("Flip"), 0,                     flip_tip,                  m_flip, 0 );
   MENUENT(mEdit,    ZTX("Add Text"), 0,                 write_text_tip,            m_write_text, 0 );
   MENUENT(mEdit,    ZTX("Add Lines/Arrows"), 0,         write_line_tip,            m_write_line, 0 );
   MENUENT(mEdit,    ZTX("Paint Edits"), 0,              paint_edits_tip,           m_paint_edits, 0 );
   MENUENT(mEdit,    ZTX("Leverage Edits"), 0,           lever_edits_tip,           m_lever_edits, 0 );
   MENUENT(mEdit,    ZTX("Plugins"), 0,                  plugins_tip,               m_plugins, 0);

   mRep = create_popmenu();
   MENUENT(mRep,     ZTX("Sharpen"), 0,               sharpen_tip,            m_sharpen, 0 );
   MENUENT(mRep,     ZTX("Blur"), 0,                  blur_tip,               m_blur, 0 );
   MENUENT(mRep,     ZTX("Denoise"), 0,               denoise_tip,            m_denoise, 0 );
   MENUENT(mRep,     ZTX("Smart Erase"), 0,           smart_erase_tip,        m_smart_erase, 0 );
   MENUENT(mRep,     ZTX("Red Eyes"), 0,              redeye_tip,             m_redeye, 0 );
   MENUENT(mRep,     ZTX("Paint/Clone"), 0,           paint_clone_tip,        m_paint_clone, 0 );
   MENUENT(mRep,     ZTX("Blend Image"), 0,           blend_image_tip,        m_blend_image, 0 );
   MENUENT(mRep,     ZTX("Paint Transparency"), 0,    paint_transp_tip,       m_paint_transp, 0 );
   MENUENT(mRep,     ZTX("Add Transparency"), 0,      add_transp_tip,         m_add_transp, 0 );
   MENUENT(mRep,     ZTX("Color Mode"), 0,            color_mode_tip,         m_color_mode, 0 );
   MENUENT(mRep,     ZTX("Shift Colors"), 0,          shift_colors_tip,       m_shift_colors, 0 );
   MENUENT(mRep,     ZTX("Color Saturation"), 0,      color_sat_tip,          m_colorsat, 0 );
   MENUENT(mRep,     ZTX("Adjust RGB/CMY"), 0,        adjust_RGB_tip,         m_adjust_RGB, 0 );
   MENUENT(mRep,     ZTX("Adjust HSL"), 0,            adjust_HSL_tip,         m_adjust_HSL, 0 );
   MENUENT(mRep,     ZTX("Brightness Gradient"), 0,   bright_grad_tip,        m_bright_gradient, 0 );
   MENUENT(mRep,     ZTX("Local Color"), 0,           local_color_tip,        m_local_color, 0 );
   MENUENT(mRep,     ZTX("Match Colors"), 0,          match_color_tip,        m_match_color, 0 );
   MENUENT(mRep,     ZTX("Color Profile"), 0,         color_profile_tip,      m_color_profile, 0 );
   MENUENT(mRep,     ZTX("Remove Dust"), 0,           remove_dust_tip,        m_remove_dust, 0 );
   MENUENT(mRep,     ZTX("Anti-Alias"), 0,            anti_alias_tip,         m_anti_alias, 0 );
   MENUENT(mRep,     ZTX("Color Fringes"), 0,         color_fringes_tip,      m_color_fringes, 0 );
   MENUENT(mRep,     ZTX("Stuck Pixels"), 0,          stuck_pixels_tip,       m_stuck_pixels, 0 );

   mWarp = create_popmenu();
   MENUENT(mWarp,    ZTX("Unbend"), 0,                unbend_tip,             m_unbend, 0 );
   MENUENT(mWarp,    ZTX("Fix Perspective"), 0,       perspective_tip,        m_perspective, 0 );
   MENUENT(mWarp,    ZTX("Warp area"), 0,             warp_area_tip,          m_warp_area, 0 );
   MENUENT(mWarp,    ZTX("Unwarp Closeup"), 0,        unwarp_closeup_tip,     m_unwarp_closeup, 0 );
   MENUENT(mWarp,    ZTX("Warp curved"), 0,           warp_curved_tip,        m_warp_curved, 0 );
   MENUENT(mWarp,    ZTX("Warp linear"), 0,           warp_linear_tip,        m_warp_linear, 0 );
   MENUENT(mWarp,    ZTX("Warp affine"), 0,           warp_affine_tip,        m_warp_affine, 0 );
   MENUENT(mWarp,    ZTX("Flatten Book Page"), 0,     flatbook_tip,           m_flatbook, 0 );
   MENUENT(mWarp,    ZTX("Spherical Projection"), 0,  sphere_tip,             m_sphere, 0);
   MENUENT(mWarp,    ZTX("Selective Rescale"), 0,     selective_rescale_tip,  m_selective_rescale, 0);
   MENUENT(mWarp,    ZTX("Make Waves"), 0,            waves_tip,              m_waves, 0);

   mEff = create_popmenu();
   MENUENT(mEff,     ZTX("Color Depth"), 0,           colordep_tip,        m_colordep, 0 );
   MENUENT(mEff,     ZTX("Sketch"), 0,                sketch_tip,          m_sketch, 0 );
   MENUENT(mEff,     ZTX("Cartoon"), 0,               cartoon_tip,         m_cartoon, 0 );
   MENUENT(mEff,     ZTX("Line Drawing"), 0,          linedraw_tip,        m_linedraw, 0 );
   MENUENT(mEff,     ZTX("Color Drawing"), 0,         colordraw_tip,       m_colordraw, 0 );
   MENUENT(mEff,     ZTX("Graduated Blur"), 0,        gradblur_tip,        m_gradblur, 0 );
   MENUENT(mEff,     ZTX("Embossing"), 0,             emboss_tip,          m_emboss, 0 );
   MENUENT(mEff,     ZTX("Tiles"), 0,                 tiles_tip,           m_tiles, 0 );
   MENUENT(mEff,     ZTX("Dots"), 0,                  dots_tip,            m_dots, 0 );
   MENUENT(mEff,     ZTX("Painting"), 0,              painting_tip,        m_painting, 0 );
   MENUENT(mEff,     ZTX("Vignette"), 0,              vignette_tip,        m_vignette, 0 );
   MENUENT(mEff,     ZTX("Texture"), 0,               texture_tip,         m_texture, 0 );
   MENUENT(mEff,     ZTX("Pattern"), 0,               pattern_tip,         m_pattern, 0 );
   MENUENT(mEff,     ZTX("Mosaic"), 0,                mosaic_tip,          m_mosaic, 0);
   MENUENT(mEff,     ZTX("Custom Kernel"), 0,         anykernel_tip,       m_anykernel, 0);
   MENUENT(mEff,     ZTX("Directed Blur"), 0,         dirblur_tip,         m_dirblur, 0);
   MENUENT(mEff,     ZTX("Blur Background"), 0,       blur_BG_tip,         m_blur_BG, 0);

   mComb = create_popmenu();
   MENUENT(mComb,    ZTX("High Dynamic Range"), 0,       HDR_tip,             m_HDR, 0 );
   MENUENT(mComb,    ZTX("High Depth of Field"), 0,      HDF_tip,             m_HDF, 0 );
   MENUENT(mComb,    ZTX("Stack/Paint"), 0,              STP_tip,             m_STP, 0 );
   MENUENT(mComb,    ZTX("Stack/Noise"), 0,              STN_tip,             m_STN, 0 );
   MENUENT(mComb,    ZTX("Panorama"), 0,                 pano_tip,            m_pano, 0 );
   MENUENT(mComb,    ZTX("Vertical Panorama"), 0,        vpano_tip,           m_vpano, 0 );
   MENUENT(mComb,    ZTX("PT Panorama"), 0,              pano_PT_tip,         m_pano_PT, 0 );
   MENUENT(mComb,    ZTX("Mashup"), 0,                   mashup_tip,          m_mashup, 0 );

   mProc = create_popmenu();
   MENUENT(mProc,    ZTX("Batch Convert"), 0,               batch_convert_tip,            m_batch_convert, 0 );
   MENUENT(mProc,    ZTX("Batch Upright"), 0,               batch_upright_tip,            m_batch_upright, 0 );
   MENUENT(mProc,    ZTX("Batch Delete/Trash"), 0,          batch_deltrash_tip,           m_batch_deltrash, 0 );
   MENUENT(mProc,    ZTX("Batch RAW"), 0,                   batch_raw_tip,                m_batch_raw, 0 );            //  16.07
   MENUENT(mProc,    ZTX("Batch Raw Therapee"), 0,          batch_rawtherapee_tip,        m_batch_rawtherapee, 0 );    //  16.07
   MENUENT(mProc,    ZTX("Script Files"), 0,                scriptfiles_tip,              m_scriptfiles, 0 );
   MENUENT(mProc,    ZTX("Burn Images to DVD/BlueRay"), 0,  burn_tip,                     m_burn, 0 );
   MENUENT(mProc,    ZTX("Find Duplicate Images"), 0,       duplicates_tip,               m_duplicates, 0 );
   MENUENT(mProc,    ZTX("Export File List"), 0,            export_filelist_tip,          m_export_filelist, 0 );
   // duplicates of metadata menu follow:
   MENUENT(mProc,    ZTX("Batch Add/Remove Tags"), 0,       batch_tags_tip,               m_batch_tags, 0 );
   MENUENT(mProc,    ZTX("Batch Rename Tags"), 0,           batch_rename_tags_tip,        m_batch_rename_tags, 0 );
   MENUENT(mProc,    ZTX("Batch Add/Change Metadata"), 0,   batch_change_metadata_tip,    m_batch_change_metadata, 0 );
   MENUENT(mProc,    ZTX("Batch Report Metadata"), 0,       batch_report_metadata_tip,    m_batch_report_metadata, 0 );
   MENUENT(mProc,    ZTX("Batch Geotags"), 0,               batch_geotags_tip,            m_batch_geotags, 0 );
   MENUENT(mProc,    ZTX("Image Locations/Dates"),  0,      locations_tip,                m_locations, 0 );
   MENUENT(mProc,    ZTX("Image Timeline"), 0,              timeline_tip,                 m_timeline, 0 );
   MENUENT(mProc,    ZTX("Search Images"),     0,           search_images_tip,            m_search_images, 0 );

   mTools = create_popmenu();
   MENUENT(mTools,   ZTX("Index Image Files"), 0,           index_tip,                 m_index, 0 );
   MENUENT(mTools,   ZTX("User Settings"), 0,               settings_tip,              m_settings, 0 );
   MENUENT(mTools,   ZTX("Keyboard Shortcuts"), 0,          KBshortcuts_tip,           m_KBshortcuts, 0 );
   MENUENT(mTools,   ZTX("Brightness Graph"), 0,            show_brdist_tip,           m_show_brdist, 0 );
   MENUENT(mTools,   ZTX("Grid Lines"), 0,                  gridlines_tip,             m_gridlines, 0 );
   MENUENT(mTools,   ZTX("Line Color"), 0,                  line_color_tip,            m_line_color, 0 );
   MENUENT(mTools,   ZTX("Show RGB"), 0,                    show_RGB_tip,              m_show_RGB, 0 );
   MENUENT(mTools,   ZTX("Magnify Image"), 0,               magnify_tip,               m_magnify, 0 );
   MENUENT(mTools,   ZTX("Dark/Bright Pixels"), 0,          darkbrite_tip,             m_darkbrite, 0 );
   MENUENT(mTools,   ZTX("Monitor Color"), 0,               moncolor_tip,              m_moncolor, 0 );
   MENUENT(mTools,   ZTX("Monitor Gamma"), 0,               mongamma_tip,              m_mongamma, 0 );
   MENUENT(mTools,   ZTX("Change Language"), 0,             changelang_tip,            m_changelang, 0 );
   MENUENT(mTools,   ZTX("Missing Translations"), 0,        untranslated_tip,          m_untranslated, 0 );
   MENUENT(mTools,   ZTX("Calibrate Printer"), 0,           calibrate_printer_tip,     m_calibrate_printer, 0 );
   MENUENT(mTools,   ZTX("Set Desktop Wallpaper"), 0,       setdesktop_tip,            m_setdesktop, 0 );
   MENUENT(mTools,   ZTX("Cycle Desktop Wallpaper"), 0,     cycledesktop_tip,          m_cycledesktop, 0 );
   MENUENT(mTools,   ZTX("Resources"), 0,                   resources_tip,             m_resources, 0 );
   MENUENT(mTools,       "zappcrash test", 0,               zappcrash_tip,             m_zappcrash, 0 ); 

   mHelp = create_popmenu();
   MENUENT(mHelp,    ZTX("Quick Start"), 0,              quick_start_tip,           m_help, 0 );
   MENUENT(mHelp,    ZTX("User Guide"), 0,               user_guide_tip,            m_help, 0 );
   MENUENT(mHelp,    ZTX("User Guide Changes"), 0,       user_guide_changes_tip,    m_help, 0 );
   MENUENT(mHelp,    ZTX("Edit Functions Summary"), 0,   edit_funcs_summary_tip,    m_help, 0 );
   MENUENT(mHelp,    ZTX("README"), 0,                   readme_tip,                m_help, 0 );
   MENUENT(mHelp,    ZTX("Change Log"), 0,               changelog_tip,             m_help, 0 );
   MENUENT(mHelp,    ZTX("Log File"), 0,                 logfile_tip,               m_help, 0 );
   MENUENT(mHelp,    ZTX("Translations"), 0,             translations_tip,          m_help, 0 );
   MENUENT(mHelp,    ZTX("Home Page"), 0,                homepage_tip,              m_help, 0 );
   MENUENT(mHelp,    ZTX("About"), 0,                    about_tip,                 m_help, 0 );

   mGmenu = create_popmenu();
   MENUENT(mGmenu,   ZTX("All Directories"), 0,             alldirs_tip,               m_alldirs, 0 );
   MENUENT(mGmenu,   ZTX("Sync Gallery"), 0,                sync_gallery_tip,          m_sync_gallery, 0 );
   MENUENT(mGmenu,   ZTX("Export Image Files"), 0,          export_images_tip,         m_export_images, 0 );
   MENUENT(mGmenu,   ZTX("Upload to Flickr"), 0,            flickr_upload_tip,         m_flickr_upload, 0 );
   MENUENT(mGmenu,   ZTX("Manage Albums"), 0,               manage_albums_tip,         m_manage_albums, 0 );
   MENUENT(mGmenu,   ZTX("Update Album Files"), 0,          update_albums_tip,         m_update_albums, 0 );
   MENUENT(mGmenu,   ZTX("Replace Album File"), 0,          replace_album_file_tip,    m_replace_album_file, 0 );
   MENUENT(mGmenu,   ZTX("Slide Show"), 0,                  slideshow_tip,             m_slideshow, 0 );

   //  F view menu (buttons) 

   MENUENT(0,  Bimage,              "viewF-check.png",   imagefile_tip,       m_viewmode, "F" );
   MENUENT(0,  ZTX("Gallery"),      "viewG.png",         gallery_tip,         m_viewmode, "G" );
   MENUENT(0,  ZTX("World Maps"),   "viewW.png",         worldmaps_tip,       m_viewmode, "W" );
   MENUENT(0,  ZTX("Net Maps"),     "viewM.png",         netmaps_tip,         m_viewmode, "M" );                    //  16.05
   MENUENT(0,  0,                   "separator.png",     0,                   0,           0  );
   MENUENT(0,  ZTX("Favorites"),    "favorites.png",     favorites_tip,       m_favorites, 0 );
   MENUENT(0,  ZTX("File"),         "file.png",          file_tip,            (cbFunc *) popup_menu, (cchar *) mFile);
   MENUENT(0,  ZTX("Save"),         "save.png",          save_tip,            m_file_save, 0 );
   MENUENT(0,  ZTX("Prev/Next"),    "prev_next.png",     prev_next_tip,       m_prev_next, 0 );
   MENUENT(0,      "Metadata",      "metadata.png",      metadata_tip,        (cbFunc *) popup_menu, (cchar *) mMeta);
   MENUENT(0,  ZTX("Areas"),        "areas.png",         areas_tip,           (cbFunc *) popup_menu, (cchar *) mArea);
   MENUENT(0,  ZTX("Edit"),         "edit.png",          edit_tip,            (cbFunc *) popup_menu, (cchar *) mEdit);
   MENUENT(0,  ZTX("Repair"),       "repair.png",        repair_tip,          (cbFunc *) popup_menu, (cchar *) mRep);
   MENUENT(0,  ZTX("Warp"),         "warp.png",          warp_tip,            (cbFunc *) popup_menu, (cchar *) mWarp);
   MENUENT(0,  ZTX("Effects"),      "effects.png",       effects_tip,         (cbFunc *) popup_menu, (cchar *) mEff);
   MENUENT(0,  ZTX("Combine"),      "combine.png",       combine_tip,         (cbFunc *) popup_menu, (cchar *) mComb);
   MENUENT(0,  ZTX("Undo/Redo"),    "undo_redo.png",     undo_redo_tip,       m_undo_redo, 0 );
   MENUENT(0,  ZTX("Process"),      "process.png",       process_tip,         (cbFunc *) popup_menu, (cchar *) mProc);
   MENUENT(0,  ZTX("Tools"),        "tools.png",         tools_tip,           (cbFunc *) popup_menu, (cchar *) mTools);
   MENUENT(0,  ZTX("Help"),         "help.png",          help_tip,            (cbFunc *) popup_menu, (cchar *) mHelp);

   NFmenu = Nmenus;                                                              //  end of F view menus

   //  G view menu (buttons)

   MENUENT(0,  Bimage,              "viewF.png",         imagefile_tip,          m_viewmode, "F" );
   MENUENT(0,  ZTX("Gallery"),      "viewG-check.png",   gallery_tip,            m_viewmode, "G" );
   MENUENT(0,  ZTX("World Maps"),   "viewW.png",         worldmaps_tip,          m_viewmode, "W" );
   MENUENT(0,  ZTX("Net Maps"),     "viewM.png",         netmaps_tip,            m_viewmode, "M" );                 //  16.05
   MENUENT(0,  0,                   "separator.png",     0,                      0,           0  );
   MENUENT(0,  ZTX("Favorites"),    "favorites.png",     favorites_tip,          m_favorites, 0 );
   MENUENT(0,      "Menu",          "gmenu.png",         gmenu_tip,              (cbFunc *) popup_menu, (cchar *) mGmenu);
   MENUENT(0,  ZTX("Bookmarks"),    "goto.png",          bookmarks_tip,          m_goto_bookmark, 0 );
   MENUENT(0,  ZTX("Zoom+"),        "zoom+.png",         thumb_increase_tip,     navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Zoom-"),        "zoom-.png",         thumb_reduce_tip,       navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Sort"),         "sort.png",          sort_order_tip,         navi::menufuncx, 0 );
   MENUENT(0,  ZTX("First"),        "top.png",           jump_begin_tip,         navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Last"),         "bottom.png",        jump_end_tip,           navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Page↑"),       "up.png",            previous_page_tip,      navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Page↓"),       "down.png",          next_page_tip,          navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Scroll"),       "scroll.png",        slow_scroll_tip,        navi::menufuncx, 0 );
   MENUENT(0,  ZTX("Process"),      "process.png",       process_tip,            (cbFunc *) popup_menu, (cchar *) mProc);
   MENUENT(0,  ZTX("Tools"),        "tools.png",         tools_tip,              (cbFunc *) popup_menu, (cchar *) mTools);
   MENUENT(0,  ZTX("Help"),         "help.png",          help_tip,               (cbFunc *) popup_menu, (cchar *) mHelp);

   NGmenu = Nmenus;                                                              //  end of G view menus

   //  W view menu (buttons)

   MENUENT(0,  Bimage,                    "viewF.png",            imagefile_tip,       m_viewmode, "F" );
   MENUENT(0,  ZTX("Gallery"),            "viewG.png",            gallery_tip,         m_viewmode, "G" );
   MENUENT(0,  ZTX("World Maps"),         "viewW-check.png",      worldmaps_tip,       m_viewmode, "W" );
   MENUENT(0,  ZTX("Net Maps"),           "viewM.png",            netmaps_tip,         m_viewmode, "M" );           //  16.05
   MENUENT(0,  0,                         "separator.png",        0,                   0,           0  );
   MENUENT(0,  ZTX("Choose Map"),         "choosemap.png",        load_filemap_tip,    m_load_filemap, 0 );
   MENUENT(0,  ZTX("Set Map Markers"),    "viewW-dots.png",       mark_filemap_tip,    m_set_map_markers, 0 );      //  17.01

   NWmenu = Nmenus;                                                              //  end of W view menus

   //  M view menu (buttons)                                                                                        //  16.05

   MENUENT(0,  Bimage,                    "viewF.png",            imagefile_tip,       m_viewmode, "F" );
   MENUENT(0,  ZTX("Gallery"),            "viewG.png",            gallery_tip,         m_viewmode, "G" );
   MENUENT(0,  ZTX("World Maps"),         "viewW.png",            worldmaps_tip,       m_viewmode, "W" );
   MENUENT(0,  ZTX("Net Maps"),           "viewM-check.png",      netmaps_tip,         m_viewmode, "M" );
   MENUENT(0,  0,                         "separator.png",        0,                   0,           0  );
   MENUENT(0,  ZTX("Choose Map"),         "choosemap.png",        netmap_source_tip,   m_netmap_source, 0 );
   MENUENT(0,  ZTX("Set Map Markers"),    "viewM-dots.png",       mark_netmap_tip,     m_set_map_markers, 0 );      //  17.01

   NMmenu = Nmenus;                                                              //  end of M view menus

   //  dummy menus only for KB shortcuts

   MENUENT(0,  ZTX("Undo"),               "KB shortcut",    0,    m_undo, 0 );
   MENUENT(0,  ZTX("Redo"),               "KB shortcut",    0,    m_redo, 0 );
   MENUENT(0,  ZTX("Save File Version"),  "KB shortcut",    0,    m_file_save_version, 0);

   //  build the vertical menus for the F/G/W/M windows

   Vmenu *Fvm = Vmenu_new(Fmenu);
   Vmenu *Gvm = Vmenu_new(Gmenu);
   Vmenu *Wvm = Vmenu_new(Wmenu);
   Vmenu *Mvm = Vmenu_new(Mmenu);                                                                                   //  16.05
   Vmenu *Xvm;

   int   iz = iconsize;

   for (me = 0; me < Nmenus; me++)
   {
      cchar *pp = menutab[me].icon;                                              //  skip entries for KB shortcuts
      if (pp && strmatch(pp,"KB shortcut")) continue;

      if (me < NFmenu) Xvm = Fvm;                                                //  F-view menu
      else if (me < NGmenu) Xvm = Gvm;                                           //  G-view
      else if (me < NWmenu) Xvm = Wvm;                                           //  W-view                             16.05
      else Xvm = Mvm;                                                            //  M-view                             16.05

      if (menutab[me].topmenu)                                                   //  submenu within top menu
         add_popmenu_item(menutab[me].topmenu, menutab[me].menu,
                menutab[me].func, menutab[me].arg, menutab[me].desc);
      else {                                                                     //  top menu
         if (strmatch(menutab[me].icon,"separator.png")) 
            Vmenu_add(Xvm, 0, menutab[me].icon, iz, iz/3, 0, 0, 0);
         else if (strmatch(menu_style,"icons")) {
            if (menutab[me].icon)
               Vmenu_add(Xvm, 0, menutab[me].icon,iz,iz,menutab[me].desc,
                                 menutab[me].func, menutab[me].arg);
            else
               Vmenu_add(Xvm, menutab[me].menu, 0, 0, 0, menutab[me].desc,
                              menutab[me].func, menutab[me].arg);
         }
         else  /* menus and icons */
            Vmenu_add(Xvm, menutab[me].menu, menutab[me].icon, iz, iz,
                           menutab[me].desc, menutab[me].func, menutab[me].arg);
      }
   }

   //  right-click popup menus --------------------------------------------

   cchar    *menupopimage = ZTX("Popup Image");
   cchar    *menumeta1 = ZTX("View Metadata");
   cchar    *menumeta2 = ZTX("Edit Metadata");
   cchar    *menumeta3 = ZTX("Edit Any Metadata");
   cchar    *menurename = ZTX("Rename");
   cchar    *menucopymove = ZTX("Copy/Move to Location");
   cchar    *menucopytodesktop = ZTX("Copy to Desktop");
   cchar    *menucopytoclip = ZTX("Copy to Clipboard");
   cchar    *menuremovefromalbum = ZTX("Remove from Album");
   cchar    *menucuttocache = ZTX("Cut to Image Cache");
   cchar    *menucopytocache = ZTX("Copy to Image Cache");
   cchar    *menupastecachehere = ZTX("Paste Image Cache Here (clear)");
   cchar    *menupastecachekeep = ZTX("Paste Image Cache Here (keep)");
   cchar    *menutrimrotate = ZTX("Trim/Rotate");
   cchar    *menuresize = ZTX("Resize");
   cchar    *menuupright = ZTX("Upright");
   cchar    *menuvoodoo1 = "Voodoo 1";
   cchar    *menuvoodoo2 = "Voodoo 2";
   cchar    *menucombo = ZTX("Retouch Combo");
   cchar    *menubrightdist = ZTX("Edit Brightness");
   cchar    *menuzonalflatten = ZTX("Zonal Flatten");
   cchar    *menutonemap = ZTX("Tone Mapping");
   cchar    *menuselect = ZTX("Select Area");
   cchar    *menuopenraw = ZTX("Open RAW file (Raw Therapee)");
   cchar    *menunetzoom = ZTX("Show on Net Map");
   cchar    *menudeltrash = ZTX("Delete/Trash ...");

   popmenu_main = create_popmenu();                                              //  main window popup menu
   add_popmenu_item(popmenu_main,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_main,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_main,menumeta3,popup_menufunc,"edit any metadata");
   add_popmenu_item(popmenu_main,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_main,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_main,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_main,menucopytocache,popup_menufunc,"copytocache");
   add_popmenu_item(popmenu_main,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_main,menuupright,popup_menufunc,"upright");
   add_popmenu_item(popmenu_main,menutrimrotate,popup_menufunc,"trim/rotate");
   add_popmenu_item(popmenu_main,menuresize,popup_menufunc,"resize");
   add_popmenu_item(popmenu_main,menuvoodoo1,popup_menufunc,"voodoo1");
   add_popmenu_item(popmenu_main,menuvoodoo2,popup_menufunc,"voodoo2");
   add_popmenu_item(popmenu_main,menucombo,popup_menufunc,"combo");
   add_popmenu_item(popmenu_main,menubrightdist,popup_menufunc,"edit brightness");
   add_popmenu_item(popmenu_main,menuzonalflatten,popup_menufunc,"zonal flatten");
   add_popmenu_item(popmenu_main,menutonemap,popup_menufunc,"tonemap");
   add_popmenu_item(popmenu_main,menuselect,popup_menufunc,"select");
   add_popmenu_item(popmenu_main,menunetzoom,popup_menufunc,"net_zoomin");
   add_popmenu_item(popmenu_main,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_thumb = create_popmenu();                                             //  gallery thumbnail popup menu
   add_popmenu_item(popmenu_thumb,menupopimage,popup_menufunc,"popimage");
   add_popmenu_item(popmenu_thumb,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_thumb,menumeta2,popup_menufunc,"edit metadata");
   add_popmenu_item(popmenu_thumb,menumeta3,popup_menufunc,"edit any metadata");
   add_popmenu_item(popmenu_thumb,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_thumb,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_thumb,menucopytodesktop,popup_menufunc,"copytodesktop");
   add_popmenu_item(popmenu_thumb,menucopytocache,popup_menufunc,"copytocache");
   add_popmenu_item(popmenu_thumb,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_thumb,menuupright,popup_menufunc,"upright");
   add_popmenu_item(popmenu_thumb,menunetzoom,popup_menufunc,"net_zoomin");
   add_popmenu_item(popmenu_thumb,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_raw = create_popmenu();                                               //  gallery thumbnail-RAW popup menu
   add_popmenu_item(popmenu_raw,menupopimage,popup_menufunc,"popimage");         //  16.09
   add_popmenu_item(popmenu_raw,menuopenraw,m_rawtherapee,0);
   add_popmenu_item(popmenu_raw,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_raw,menurename,popup_menufunc,"rename");
   add_popmenu_item(popmenu_raw,menucopymove,popup_menufunc,"copymove");
   add_popmenu_item(popmenu_raw,menudeltrash,popup_menufunc,"delete/trash");

   popmenu_album = create_popmenu();                                             //  album gallery thumbnail popup menu
   add_popmenu_item(popmenu_album,menupopimage,popup_menufunc,"popimage");
   add_popmenu_item(popmenu_album,menucopytocache,popup_menufunc,"copytocache");
   add_popmenu_item(popmenu_album,menucuttocache,popup_menufunc,"cuttocache");
   add_popmenu_item(popmenu_album,menucopytoclip,popup_menufunc,"copytoclip");
   add_popmenu_item(popmenu_album,menupastecachekeep,popup_menufunc,"pastecachekeep");
   add_popmenu_item(popmenu_album,menupastecachehere,popup_menufunc,"pastecachehere");
   add_popmenu_item(popmenu_album,menuremovefromalbum,popup_menufunc,"removefromalbum");
   add_popmenu_item(popmenu_album,menupopimage,popup_menufunc,"popimage");
   add_popmenu_item(popmenu_album,menumeta1,popup_menufunc,"view metadata");
   add_popmenu_item(popmenu_album,menucopytodesktop,popup_menufunc,"copytodesktop");

   return;
}


//  right-click popup menu function

void popup_menufunc(GtkWidget *, cchar *menu)
{
   if (strmatch(menu,"popimage")) gallery_popimage();                            //  funcs for main and gallery windows
   if (strmatch(menu,"view metadata")) meta_view(1);
   if (strmatch(menu,"edit metadata")) m_edit_metadata(0,0);
   if (strmatch(menu,"edit any metadata")) m_meta_edit_any(0,0);                 //  16.06
   if (strmatch(menu,"rename")) m_rename(0,0);                                   //  these use clicked_file if defined,
   if (strmatch(menu,"upright")) m_upright(0,0);                                 //    else they use curr_file.
   if (strmatch(menu,"copymove")) m_copy_move(0,0);                              //  16.06
   if (strmatch(menu,"copytodesktop")) m_copyto_desktop(0,0);                    //  16.09
   if (strmatch(menu,"delete/trash")) m_delete_trash(0,0);                       //  16.06
   if (strmatch(menu,"net_zoomin")) m_net_zoomin(0,0);                           //  16.06
   if (strmatch(menu,"copytoclip")) m_copyto_clip(0,0);
   if (strmatch(menu,"copytocache")) m_copyto_cache(0,0);

   if (strmatch(menu,"removefromalbum")) m_album_removefile(0,0);                //  funcs for album gallery
   if (strmatch(menu,"cuttocache")) m_album_cutfile(0,0);                        //  depend on clicked_file being defined
   if (strmatch(menu,"pastecachehere")) m_album_pastecache(0,"clear");
   if (strmatch(menu,"pastecachekeep")) m_album_pastecache(0,"keep");

   if (strmatch(menu,"trim/rotate")) m_trimrotate(0,0);                          //  functions using curr_file only
   if (strmatch(menu,"resize")) m_resize(0,0);                                   //  (not used for gallery/thumbnail click)
   if (strmatch(menu,"voodoo1")) m_voodoo1(0,0);
   if (strmatch(menu,"voodoo2")) m_voodoo2(0,0);
   if (strmatch(menu,"combo")) m_combo(0,0);
   if (strmatch(menu,"edit brightness")) m_britedist(0,0);
   if (strmatch(menu,"zonal flatten")) m_zonal_flatten(0,0);
   if (strmatch(menu,"tonemap")) m_tonemap(0,0);
   if (strmatch(menu,"select")) m_select(0,0);

   return;
}


//  main window mouse right-click popup menu

void image_Rclick_popup()
{
   if (! curr_file) return;
   popup_menu(Mwin,popmenu_main);
   return;
}


//  gallery thumbnail mouse left-click function
//  open the clicked file in view mode F

void gallery_Lclick_func(int Nth)
{
   char     *file;

   if (clicked_file) {                                                           //  lose memory of clicked thumbnail   16.09
      zfree(clicked_file);
      clicked_file = 0;
   }
   if (checkpend("busy block mods")) return;
   file = gallery(0,"find",Nth);
   if (! file) return;
   f_open(file,Nth,0,1);                                                         //  clicked file >> current file
   zfree(file);
   m_viewmode(0,"F");
   return;
}


//  gallery thumbnail mouse right-click popup menu

void gallery_Rclick_popup(int Nth)
{
   FTYPE    ftype;

   clicked_posn = Nth;                                                           //  clicked gallery position (0 base)
   clicked_file = gallery(0,"find",Nth);                                         //  clicked_file is subject for zfree()
   if (! clicked_file) return;
   ftype = image_file_type(clicked_file);

   if (navi::gallerytype == ALBUM)                                               //  gallery type is album
      popup_menu(Mwin,popmenu_album);

   else if (ftype == IMAGE)                                                      //  clicked thumbnail is an image file
      popup_menu(Mwin,popmenu_thumb);

   else if (ftype == RAW)                                                        //  clicked thumbnail is a RAW file
      popup_menu(Mwin,popmenu_raw);

   return;
}


/********************************************************************************/

//  set window view mode, F/G/W/M

void m_viewmode(GtkWidget *, cchar *fgwm)
{
   zthreadcrash();

   if (FGWM == *fgwm) return;                                                    //  no change

   if (*fgwm == 'F')                                                             //  set F view mode for image file
   {
      F1_help_topic = "file_view";

      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Whbox);
      gtk_widget_hide(Mhbox);                                                    //  16.05
      gtk_widget_show_all(Fhbox);
      set_mwin_title();
      FGWM = 'F';

      Cstate = &Fstate;                                                          //  set drawing area
      Cdrawin = Fdrawin;
      gdkwin = gtk_widget_get_window(Fdrawin);                                   //  GDK window

      if (zddeltrash) m_delete_trash(0,0);                                       //  reset target file for funcs in     16.06
      if (zdcopymove) m_copy_move(0,0);                                          //    gallery thumbnail popup menus
      if (zdexifview) meta_view(0);
      if (zdrename) m_rename(0,0);
   }

   if (*fgwm == 'G')                                                             //  set G view mode for thumbnail gallery
   {
      if (CEF) return;                                                           //  don't interrupt edit func.
      if (Fslideshow) return;                                                    //  no crash slide show

      gtk_widget_hide(Fhbox);
      gtk_widget_hide(Whbox);
      gtk_widget_hide(Mhbox);                                                    //  16.05
      gtk_widget_show_all(Ghbox);
      FGWM = 'G';

      Cstate = 0;                                                                //  no F/W image drawing area
      Cdrawin = 0;
      gdkwin = 0;

      if (curr_file) gallery(curr_file,"paint");                                 //  set gallery posn. at curr. file
      else gallery(0,"paint",-1);                                                //  else leave unchanged
   }

   if (*fgwm == 'W')                                                             //  set W view mode for file maps
   {
      if (CEF) return;                                                           //  don't interrupt edit func.
      if (Fslideshow) return;                                                    //  no crash slide show

      F1_help_topic = "worldmap_menus";

      gtk_widget_hide(Fhbox);                                  
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Mhbox);                                                    //  16.05
      gtk_widget_show_all(Whbox);
      FGWM = 'W';

      Cstate = &Wstate;                                                          //  set drawing area
      Cdrawin = Wdrawin;
      gdkwin = gtk_widget_get_window(Wdrawin);                                   //  GDK window

      if (! Wstate.fpxb) m_load_filemap(0,"default");                            //  no map loaded, load default map 
      gtk_window_set_title(MWIN,ZTX("Image Locations"));                         //  window title
      Fpaintnow();
   }

   if (*fgwm == 'M')                                                             //  16.05
   {
      if (CEF) return;                                                           //  don't interrupt edit func.
      if (Fslideshow) return;                                                    //  no crash slide show

      F1_help_topic = "netmap_menus";

      gtk_widget_hide(Fhbox);                                  
      gtk_widget_hide(Ghbox);
      gtk_widget_hide(Whbox);
      gtk_widget_show_all(Mhbox);
      FGWM = 'M';

      Cstate = 0;                                                                //  no F/W image drawing area
      Cdrawin = 0;
      gdkwin = 0;
      
      m_load_netmap(0,"init");                                                   //  load net initial map
      gtk_window_set_title(MWIN,ZTX("Image Locations"));                         //  window title
   }

   return;
}


/********************************************************************************/

//  favorites menu - popup graphic menu with user's favorites

void m_favorites(GtkWidget *, cchar *)
{
   void  favorites_callback(cchar *menu);
   char  menuconfigfile[200];

   F1_help_topic = "favorites_menu";
   snprintf(menuconfigfile,200,"%s/menu-config",favorites_dirk);
   gmenuz(Mwin,ZTX("Favorites"),menuconfigfile,favorites_callback);
   return;
}


//  response function for clicked menu
//  a menu function is called as from the text menus

void favorites_callback(cchar *menu)
{
   int      ii;

   if (! menu) return;                                                           //  16.04
   if (strmatchcase(menu,"quit")) return;

   if (strmatchcase(menu,"help")) {                                              //  16.04
      showz_userguide("favorites_menu");
      return;
   }
   
   for (ii = 0; ii < Nmenus; ii++) {
      if (! menutab[ii].menu) continue;                                          //  16.04
      if (strmatchcase(ZTX(menu),ZTX(menutab[ii].menu))) break;
   }

   if (ii < Nmenus) menutab[ii].func(0,menu);
   return;
}



